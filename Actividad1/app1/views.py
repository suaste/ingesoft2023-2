from django.shortcuts import render, HttpResponse
from .models import *
from django.db import connection

# Create your views here.
def index(request):

    estudiantes_grupo1 = Estudiante.objects.filter(grupo = 1)
    estudiantes_grupo4 = Estudiante.objects.filter(grupo = 4)
    query_mismo_apellido = "SELECT * FROM app1_estudiante WHERE apellidos IN (SELECT apellidos FROM app1_estudiante GROUP BY apellidos HAVING COUNT(*) > 1) ORDER BY apellidos;"
    query_misma_edad = "SELECT * FROM app1_estudiante WHERE edad IN (SELECT edad FROM app1_estudiante GROUP BY edad HAVING COUNT(*) > 1) ORDER BY edad;"
    query_grupo3_misma_edad = "SELECT * FROM app1_estudiante WHERE grupo_id = 3 AND edad IN (SELECT edad FROM app1_estudiante WHERE grupo_id = 3 GROUP BY edad HAVING COUNT(*) > 1) ORDER BY edad;"
    mismo_apellido = Estudiante.objects.raw(query_mismo_apellido)
    misma_edad = Estudiante.objects.raw(query_misma_edad)
    grupo3_misma_edad = Estudiante.objects.raw(query_grupo3_misma_edad)
    todos = Estudiante.objects.all()

    return render(request, 'index.html', {'estudiantes_grupo1':estudiantes_grupo1, 'estudiantes_grupo4':estudiantes_grupo4, 'mismo_apellido':mismo_apellido, 'misma_edad':misma_edad, 'grupo3_misma_edad':grupo3_misma_edad, 'todos':todos})
